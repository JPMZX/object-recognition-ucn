package com.emaraic.ObjectRecognition;

import com.esotericsoftware.tablelayout.swing.Table;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.tensorflow.DataType;
import org.tensorflow.Graph;
import org.tensorflow.Output;
import org.tensorflow.Session;
import org.tensorflow.Tensor;

/**
 *
 * @author Taha Emara 
 * Website: http://www.emaraic.com 
 * Email : taha@emaraic.com
 * Created on: Apr 29, 2017
 * Kindly: Don't remove this header
 * Download the pre-trained inception model from here: https://storage.googleapis.com/download.tensorflow.org/models/inception_dec_2015.zip 
 */
public class Recognizer extends JFrame implements ActionListener {


    private Table table;
    private JButton predict;
    private JButton incep;
    private JButton img;
    private JFileChooser incepch;
    private JFileChooser imgch;
    private JLabel viewer;
    private JTextField result;
    private JTextField imgpth;
    private JTextField modelpth;
    private FileNameExtensionFilter imgfilter = new FileNameExtensionFilter(
            "Imagenes JPG y JPEG", "jpg", "jpeg");
    private String modelpath;
    private String imagepath;
    private boolean modelselected = false;
    private byte[] graphDef;
    private List<String> labels;

    public Recognizer() {
        setTitle("Reconocimiento de objectos - Actividad UCN");
        setSize(500, 600); // Primer valor tamaño en eje X, segundo valor tamaño en Y
        table = new Table();
        predict = new JButton("Predecir"); // Texto botón predecir
        predict.setEnabled(false);  // Se inhabilita inialmente hasta que el usuario elija un inception e imagen
        incep = new JButton("Elegir dir. de Inception"); // Texto botón elegir inception
        img = new JButton("Elegir imágen"); // Texto botón elegir imágen
        incep.addActionListener(this);
        img.addActionListener(this);
        predict.addActionListener(this);
        
        incepch = new JFileChooser();
        imgch = new JFileChooser();
        imgch.setFileFilter(imgfilter);
        imgch.setFileSelectionMode(JFileChooser.FILES_ONLY);
        incepch.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        
        result=new JTextField();
        result.setHorizontalAlignment(JTextField.CENTER); // Se centra el texto dentro del input de resultado
        modelpth=new JTextField();
        imgpth=new JTextField();
        modelpth.setEditable(false);
        imgpth.setEditable(false);
        viewer = new JLabel();
        getContentPane().add(table);
        table.addCell(incep).width(200).padRight(15);; // Ubicación botón elegir inception
        table.addCell(modelpth).width(250);// Ubicación input elegir inception
        table.row();
        table.addCell(img).width(200).padTop(15).padRight(15); // Ubicación botón elegir imágen
        table.addCell(imgpth).width(250).padTop(15); // Ubicación input elegir imágen
        table.row();
        table.addCell(viewer).size(200, 200).colspan(2).padTop(15);
        table.row();
        table.addCell(predict).colspan(2).padTop(15);
        table.row();
        table.addCell(result).width(450).padTop(10).colspan(2);
        table.row();
        // Bloques de texto que aparecen al final del layout
        table.addCell(new JLabel("Modificado por:")).center().padTop(20).colspan(2);
        table.row();
        table.addCell(new JLabel("Juan Pablo Martinez")).center().colspan(2);
        table.row();
        table.addCell(new JLabel("Christian Restrepo")).center().colspan(2);
        table.row();
        table.addCell(new JLabel("Juan Camilo Aguilar")).center().colspan(2);
        table.row();
        table.addCell(new JLabel("Sistemas inteligentes")).center().padTop(10).colspan(2);
        table.row();
        table.addCell(new JLabel("Fundación Universitaria Católica del Norte")).center().colspan(2);
       
        setLocationRelativeTo(null);

        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    	/*
    		Este metodo captura las acciones realizadas en los botones 
    		de seleccionar inception y seleccionar imagen
    	*/
        if (e.getSource() == incep) {
            int returnVal = incepch.showOpenDialog(this); // Muestra la ventana de selección de directorios

            if (returnVal == JFileChooser.APPROVE_OPTION) { // Se ejecuta si el usuario selecciono un directorio
                File file = incepch.getSelectedFile(); // Se instancia el directorio seleccionado
                modelpath = file.getAbsolutePath(); // Obtiene el path de el directorio seleccionado
                modelpth.setText(modelpath); // Escribe esta ruta en el input del lado del boton de inception
                System.out.println("Cargando el directorio: " + file.getAbsolutePath());
                modelselected = true;
                // Intenta leer los dos archivos que se descargan con el inception, en caso de no encontrarlos cancela la carga de los archivos.
                graphDef = readAllBytesOrExit(Paths.get(modelpath, "tensorflow_inception_graph.pb"));
                labels = readAllLinesOrExit(Paths.get(modelpath, "imagenet_comp_graph_label_strings.txt"));
            } else {
                System.out.println("El usuario cancela la carga del inception.");
            }

        } else if (e.getSource() == img) {
            int returnVal = imgch.showOpenDialog(Recognizer.this); // Muestra la ventana de selección de imagenes
            if (returnVal == JFileChooser.APPROVE_OPTION) { // Se ejecuta si el usuario selecciono una imágen
                try {
                    File file = imgch.getSelectedFile(); // Se instancia el archivo seleccionado
                    imagepath = file.getAbsolutePath(); // Obtiene el path de la imágen seleccionada
                    imgpth.setText(imagepath); // Escribe esta ruta en el input del lado del boton de inception
                    System.out.println("Path de la imágen: " + imagepath);
                    Image img = ImageIO.read(file);

                    viewer.setIcon(new ImageIcon(img.getScaledInstance(200, 200, 200))); // Muestra la imagen seleccionada en pantalla
                    if (modelselected) {
                        predict.setEnabled(true); // Si se selecciona un modelo y una imagen habilita el botón de predecir
                    }
                } catch (IOException ex) {
                    Logger.getLogger(Recognizer.class.getName()).log(Level.SEVERE, null, ex); // Muestra cualquier excepción en consola.
                }
            } else {
                System.out.println("El usuario cancela la carga de la imágen."); // Si no selecciona una imágen muestra en consola un mensaje.
            }
        } else if (e.getSource() == predict) { // Evento al dar click en el botón predecir
            byte[] imageBytes = readAllBytesOrExit(Paths.get(imagepath)); // Lee los bytes de la imágen seleccionada

            try (Tensor image = Tensor.create(imageBytes)) { // Instancia la clase Tensor del paquete org.tensorflow y envía los bits de la imágen como párametro
                float[] labelProbabilities = executeInceptionGraph(graphDef, image); // Ejecuta el método pasando el modelo de inception y la imagen como parametros
                int bestLabelIdx = maxIndex(labelProbabilities); // Ejecuta el metodo para buscar la mejor probabilidad
                result.setText(""); // Borra el texto del input de resultado
                // Escribe en el input de resultado los datos obtenidos del proceso
                result.setText(String.format(
                                "Mejor coincidencia: %s (%.2f%% prob.)",
                                labels.get(bestLabelIdx), labelProbabilities[bestLabelIdx] * 100f));
                System.out.println(
                        String.format(
                                "Mejor coincidencia: %s (%.2f%% prob.)",
                                labels.get(bestLabelIdx), labelProbabilities[bestLabelIdx] * 100f));
            }

        }
    }

    ///
    private static float[] executeInceptionGraph(byte[] graphDef, Tensor image) {
        try (Graph g = new Graph()) { // Instancia la clase Graph
            g.importGraphDef(graphDef); // Importa los Graph contenidos en el inception con los modelos pre entrenados a la instancia
            try (Session s = new Session(g);  // Crea una instancia de la clase Session enviando como argumento la instancia de Graph, la session se utiliza para cachear y realizar las operaciones
                    Tensor result = s.runner().feed("DecodeJpeg/contents", image).fetch("softmax").run().get(0)) { // crea un nuevo ejecutor de procesos, decodigica la imagen pra procesarla, extrae la mejor coincidencia de los resultados, ejecuta las operaciones y accede al dato en indice 0 del resultado.
                final long[] rshape = result.shape();
                if (result.numDimensions() != 2 || rshape[0] != 1) { // Verifica que las dimensiones de la forma sean diferentes de 2 y que rshape en indice 0 sea diferente de 1
                    throw new RuntimeException( // Para la ejecución del programa explicando el tipo de error
                            String.format(
                                    "Expected model to produce a [1 N] shaped tensor where N is the number of labels, instead it produced one with shape %s",
                                    Arrays.toString(rshape)));
                }
                int nlabels = (int) rshape[1];
                return result.copyTo(new float[1][nlabels])[0]; // Retorna el array de resultados
            }
        }
    }

    private static int maxIndex(float[] probabilities) { // Selecciona el resultado con la probabilidad mas alta y lo retorna.
        int best = 0;
        for (int i = 1; i < probabilities.length; ++i) {
            if (probabilities[i] > probabilities[best]) {
                best = i;
            }
        }
        return best;
    }

    private static byte[] readAllBytesOrExit(Path path) {
        try {
            return Files.readAllBytes(path);
        } catch (IOException e) {
        	// Cancela la ejecución del programa si no logra leer los bytes del archivo
            System.err.println("Error al leer [" + path + "]: " + e.getMessage());
            System.exit(1);
        }
        return null;
    }

    private static List<String> readAllLinesOrExit(Path path) {
        try {
            return Files.readAllLines(path, Charset.forName("UTF-8"));
        } catch (IOException e) {
        	// Cancela la ejecución del programa si no logra leer las lineas del archivo
            System.err.println("Error al leer [" + path + "]: " + e.getMessage());
            System.exit(0);
        }
        return null;
    }

    // In the fullness of time, equivalents of the methods of this class should be auto-generated from
    // the OpDefs linked into libtensorflow_jni.so. That would match what is done in other languages
    // like Python, C++ and Go.
    static class GraphBuilder {

        GraphBuilder(Graph g) {
            this.g = g;
        }

        Output div(Output x, Output y) {
            return binaryOp("Div", x, y);
        }

        Output sub(Output x, Output y) {
            return binaryOp("Sub", x, y);
        }

        Output resizeBilinear(Output images, Output size) {
            return binaryOp("ResizeBilinear", images, size);
        }

        Output expandDims(Output input, Output dim) {
            return binaryOp("ExpandDims", input, dim);
        }

        Output cast(Output value, DataType dtype) {
            return g.opBuilder("Cast", "Cast").addInput(value).setAttr("DstT", dtype).build().output(0);
        }

        Output decodeJpeg(Output contents, long channels) {
            return g.opBuilder("DecodeJpeg", "DecodeJpeg")
                    .addInput(contents)
                    .setAttr("channels", channels)
                    .build()
                    .output(0);
        }

        Output constant(String name, Object value) {
            try (Tensor t = Tensor.create(value)) {
                return g.opBuilder("Const", name)
                        .setAttr("dtype", t.dataType())
                        .setAttr("value", t)
                        .build()
                        .output(0);
            }
        }

        private Output binaryOp(String type, Output in1, Output in2) {
            return g.opBuilder(type, type).addInput(in1).addInput(in2).build().output(0);
        }

        private Graph g;
    }
    ////////////

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new Recognizer().setVisible(true);

            }
        });
    }

}
